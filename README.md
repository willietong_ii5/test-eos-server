# Test EOS Server #

### Install Parity ###
We need this to talk to Ethererum. At the moment we will be using a Kovan Test Net first. You can visit https://github.com/paritytech/parity for more info on how to install.

Once installation is done, run
```
parity --chain=kovan
```


### How to Set up ###
1. Clone this repo
2. Run `npm install`
3. Make sure you have Parity installed and it is running.
4. Run `npm start`
5. Go to http://localhost:3000/eos-sales-statistic and see if you can load the data.