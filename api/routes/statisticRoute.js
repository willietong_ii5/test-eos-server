'use strict';
module.exports = function(app) {
  var statistic = require('../controllers/statisticController');


  // todoList Routes
  app.route('/eos-sales-statistic')
    .get(statistic.get_eos_sales_statistic);
    app.route('/eos-sales-statistic-for-an-account/')
      .get(statistic.get_eos_sales_statistic_for_an_account);

};
