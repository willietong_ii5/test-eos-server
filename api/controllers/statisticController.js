'use strict';

var Web3 = require('web3');
var web3;

if (typeof web3 !== 'undefined') {
  console.log("web3 undefined")
  web3 = new Web3(web3.currentProvider);
} else {
  // set the provider you want from Web3.providers
  web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
}


var eos_sale_address_kovan  = "0x584483282cfcad032eb5ff6c03260a079ab5fbc8";
var eos_token_address_kovan = "0x3b06e4e45ccc45ee21e276462ab07e8581fcf0e6";
var eos_sale_abi =
[{"constant":true,"inputs":[{"name":"","type":"uint256"},{"name":"","type":"address"}],"name":"claimed","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"owner_","type":"address"}],"name":"setOwner","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"who","type":"address"}],"name":"claim","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"},{"name":"","type":"address"}],"name":"userBuys","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"day","type":"uint256"}],"name":"createOnDay","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"freeze","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"keys","outputs":[{"name":"","type":"bytes"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"startTime","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"authority_","type":"address"}],"name":"setAuthority","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"dailyTotals","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"key","type":"bytes"}],"name":"register","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"buy","outputs":[],"payable":true,"type":"function"},{"constant":true,"inputs":[],"name":"EOS","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"today","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"authority","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"createFirstDay","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"timestamp","type":"uint256"}],"name":"dayFor","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"day","type":"uint256"},{"name":"who","type":"address"}],"name":"claim","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"timestamp","type":"uint256"},{"name":"limit","type":"uint256"}],"name":"buyWithLimit","outputs":[],"payable":true,"type":"function"},{"constant":false,"inputs":[],"name":"collect","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"numberOfDays","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"createPerDay","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"inputs":[{"name":"numberOfDays_","type":"uint256"},{"name":"totalSupply_","type":"uint128"},{"name":"startTime_","type":"uint256"}],"payable":false,"type":"constructor"},{"payable":true,"type":"fallback"},{"anonymous":false,"inputs":[{"indexed":false,"name":"day","type":"uint256"},{"indexed":false,"name":"who","type":"address"},{"indexed":false,"name":"wad","type":"uint256"}],"name":"LogClaim","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"wad","type":"uint256"}],"name":"LogCollect","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"who","type":"address"},{"indexed":false,"name":"key","type":"bytes"}],"name":"LogRegister","type":"event"},{"anonymous":true,"inputs":[{"indexed":true,"name":"sig","type":"bytes4"},{"indexed":true,"name":"guy","type":"address"},{"indexed":true,"name":"foo","type":"bytes32"},{"indexed":true,"name":"bar","type":"bytes32"},{"indexed":false,"name":"wad","type":"uint256"},{"indexed":false,"name":"fax","type":"bytes"}],"name":"LogNote","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"authority","type":"address"}],"name":"LogSetAuthority","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"}],"name":"LogSetOwner","type":"event"}];

var eos_token_abi =
[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"bytes32"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"stop","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"guy","type":"address"},{"name":"wad","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"owner_","type":"address"}],"name":"setOwner","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"src","type":"address"},{"name":"dst","type":"address"},{"name":"wad","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"dst","type":"address"},{"name":"wad","type":"uint128"}],"name":"push","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"name_","type":"bytes32"}],"name":"setName","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"wad","type":"uint128"}],"name":"mint","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"src","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"stopped","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"authority_","type":"address"}],"name":"setAuthority","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"src","type":"address"},{"name":"wad","type":"uint128"}],"name":"pull","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"wad","type":"uint128"}],"name":"burn","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"bytes32"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"dst","type":"address"},{"name":"wad","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"start","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"authority","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"src","type":"address"},{"name":"guy","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"inputs":[{"name":"symbol_","type":"bytes32"}],"payable":false,"type":"constructor"},{"anonymous":true,"inputs":[{"indexed":true,"name":"sig","type":"bytes4"},{"indexed":true,"name":"guy","type":"address"},{"indexed":true,"name":"foo","type":"bytes32"},{"indexed":true,"name":"bar","type":"bytes32"},{"indexed":false,"name":"wad","type":"uint256"},{"indexed":false,"name":"fax","type":"bytes"}],"name":"LogNote","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"authority","type":"address"}],"name":"LogSetAuthority","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"}],"name":"LogSetOwner","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Approval","type":"event"}];

var WAD = 1000000000000000000;
var repeat     = (x, n)      => new Array(n + 1).join("x")
var iota       = n           => repeat("x", n).split("").map((_, i) => i)
var formatWad  = wad         => String(wad).replace(/\..*/, "")

var moment = require('moment')
var async = require('async')

var hopefully = (res, $) => (error, result) => {
  if (error) {
    // Error found, return an error
    console.log("error", error);
    return res.status(500).send('Hey an error here');
  } else {
    $(result)
  }
}


exports.get_eos_sales_statistic = function(req, res) {
  var eos_sale  = web3.eth.contract(eos_sale_abi).at(eos_sale_address_kovan);
  var eos_token = web3.eth.contract(eos_token_abi).at(eos_token_address_kovan);

  console.log('is connected', web3.isConnected())
  web3.eth.getBlock("latest", hopefully(res, block => {

    var time = block.timestamp
    async.parallel({
          today: $ => eos_sale.dayFor(time, $),
          days: $ => eos_sale.numberOfDays($),
          startTime: $ => eos_sale.startTime($),
        }, hopefully(res, ({ today, days, startTime }) => {
      var startMoment = moment(Number(startTime) * 1000)
      async.map(iota(Number(days) + 1), (i, $) => {
        var day = { id: i }
        eos_sale.createOnDay(day.id, hopefully(res, createOnDay => {
          eos_sale.dailyTotals(day.id, hopefully(res, dailyTotal => {
              day.name = day.id
              day.createOnDay = createOnDay.div(WAD)
              day.dailyTotal = dailyTotal.div(WAD)

              if (day.id == 0) {
                day.ends = startMoment
              } else {
                day.begins = startMoment.clone().add(23 * (day.id - 1), "hours")
                day.ends = day.begins.clone().add(23, "hours")
              }
              $(null, day)
          }))
        }))
      }, hopefully(res, days => {
          var filteredDays = days.filter((day, i) => {
            return i <= Number(today)
          }).forEach((day) => {
              console.log(formatWad(day.dailyTotal) + ' ETH')
          });
         return res.status(200).send(JSON.stringify(days));
      }))
    }))
  }))
}

exports.get_eos_sales_statistic_for_an_account = function(req, res) {

  var account = web3.eth.accounts[0]
  console.log('account is ', account)

  var eos_sale  = web3.eth.contract(eos_sale_abi).at(eos_sale_address_kovan);
  var eos_token = web3.eth.contract(eos_token_abi).at(eos_token_address_kovan);

  web3.eth.getBlock("latest", hopefully(res, block => {

    var time = block.timestamp
    async.parallel(Object.assign({
          today: $ => eos_sale.dayFor(time, $),
          days: $ => eos_sale.numberOfDays($),
          startTime: $ => eos_sale.startTime($),
        }, account ? {
          eth_balance: $ => web3.eth.getBalance(account, $),
          eos_balance: $ => eos_token.balanceOf(account, $),
          publicKey: $ => eos_sale.keys(account, $),
        } : {}), hopefully(res, ({
          today, days, startTime,
          eth_balance, eos_balance, publicKey,
        }) => {
      var startMoment = moment(Number(startTime) * 1000)
      async.map(iota(Number(days) + 1), (i, $) => {
        var day = { id: i }
        eos_sale.createOnDay(day.id, hopefully(res, createOnDay => {
          eos_sale.dailyTotals(day.id, hopefully(res, dailyTotal => {
            eos_sale.userBuys(day.id, account, hopefully(res, userBuys => {
              day.name = day.id
              day.createOnDay = createOnDay.div(WAD)
              day.dailyTotal = dailyTotal.div(WAD)
              day.userBuys = userBuys.div(WAD)
              day.price = dailyTotal.div(createOnDay)
              day.received = day.createOnDay.div(day.dailyTotal).times(day.userBuys)


              if (day.id == 0) {
                day.ends = startMoment
              } else {
                day.begins = startMoment.clone().add(23 * (day.id - 1), "hours")
                day.ends = day.begins.clone().add(23, "hours")
              }

              eos_sale.claimed(day.id, account, hopefully(res, claimed => {

                day.claimed = claimed

                $(null, day)
              }))
            }))
          }))
        }))
      }, hopefully(res, days => {
        var unclaimed = days.filter((x, i) => {
          return i < Number(today) && !x.claimed
        }).reduce((a, x) => x.received.plus(a), web3.toBigNumber(0))

        if (account) {
          console.log("Total Contributions")
          var filteredDays = days.filter((day, i) => {
            return i <= Number(today)
          }).forEach((day) => {
              console.log(formatWad(day.dailyTotal) + ' ETH')
          });
        }
       return res.status(200).send(JSON.stringify(days));
      }))
    }))

  }))


};
