var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  bodyParser = require('body-parser');

var cors = require('cors');
app.use(cors());

var routes = require('./api/routes/statisticRoute');
routes(app);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.listen(port);

console.log('EOS Statistic RESTful API server started on: ' + port);
